require(['bootstrap', 'jquery.extern'], function () {
    $(function () {

        $('#frame-menu .m-1').on('click', function () {
            if (!$(this).next().is(':visible')) {
                $('#frame-menu .m-1').next().slideUp();
                $(this).next().slideDown();
            }
            return false;
        });
        $('#frame-menu .m-2').on('click', function () {
            $(this).next().slideDown();
            return false;
        });

        admin_resize();
    });


    $(window).on('resize', admin_resize);

    function admin_resize() {
        if ($('#frame-content').height() >= $(window).height()) {
            $('#frame-menu').css({
                height: $(window).height() - 55 + 'px'
            });
        } else {
            $('#frame-menu').css({
                height: $(window).height() - 115 + 'px'
            });
        }
        $('#frame-content').css({
            minHeight: $(window).height() - 91 + 'px'
        });

    }


});

function admin_grid_add(url) {
    var u = window.location.href;
    var pos = u.indexOf('#');
    var page = 1;
    if (pos > 0) {
        var current = u.substring(pos + 1);
        if (parseInt(current)) {
            page = parseInt(current);
        }
    }
    $.redirect(url.replace('-page-', page));
    return false;
}

function admin_grid_edit(url, id) {
    var u = window.location.href;
    var pos = u.indexOf('#');
    var page = 1;
    if (pos > 0) {
        var current = u.substring(pos + 1);
        if (parseInt(current)) {
            page = parseInt(current);
        }
    }
    $.redirect(url.replace('-page-', page).replace('-id-', id));
    return false;
}

function admin_grid_delete(url) {
    var ids = [];
    grid.find('input[name=select]:checked').each(function (i, o) {
        var id = parseInt($(o).val());
        if (id) {
            ids.push(id);
        }
    });
    if (ids.length) {
        $.post(url, {ids: ids.join(',')}, function (data) {
            if (data.status && data.info && data.info == 'OK' && data.status == 1) {
                grid.bootgrid('reload');
            } else {
                $.defaultFormCallback(data);
            }
        });
    }
}

function admin_grid_delete_one(url, id) {
    $.post(url, {ids: id}, function (data) {
        if (data.status && data.info && data.info == 'OK' && data.status == 1) {
            grid.bootgrid('reload');
        } else {
            $.defaultFormCallback(data);
        }
    });
}

function admin_ajax_request(url, data) {
    var waiting = $.dialog.tips(lhgdialog_lang.LOADING, 10, 'loading.gif');
    $.post(url, data, function (data) {
        waiting.close();
        $.defaultFormCallback(data);
    });
    return false;
}