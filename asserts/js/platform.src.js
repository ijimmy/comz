var __platform = {
    win: false,
    mac: false,
    xll: false,
    is_pc: false
};
var p = navigator.platform;
__platform.win = p.indexOf("Win") == 0;
__platform.mac = p.indexOf("Mac") == 0;
__platform.x11 = (p == "X11") || (p.indexOf("Linux") == 0);
if (__platform.win || __platform.mac || __platform.xll) {
    __platform.is_pc = true;
}