<?php

namespace Admin\Controller;

use \Think\Controller;

class HelperController extends Controller
{

    public function reportlog()
    {

        if (!function_exists('curl_init')) {
            $this->error('不支持Curl库，不能发送');
        }
        $log_count = 0;
        $posts = array();
        foreach (list_file(LOG_PATH) as $f) {
            if ($f ['isDir']) {
                foreach (list_file($f ['pathname'] . '/', '*.log') as $ff) {
                    if ($ff ['isFile']) {
                        $log_count += 1;
                        $posts ['log_file_' . $log_count] = '@' . $ff ['pathname'];
                        $posts ['log_dir_' . $log_count] = $f['filename'];
                    }
                }
            }
        }

        $posts['app_domain'] = HTTP_HOST;
        $posts['app_name'] = APP_NAME;
        $posts['app_version'] = APP_VERSION;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://dog.tecmz.com/receive/reportlog");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $posts);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        $response = curl_exec($ch);
        curl_close($ch);

        if ($response && ($response = @json_decode($response, true))) {
            if (isset($response['code']) && $response['code'] == 200 && isset($response['msg']) && 'OK' == $response['msg']) {
                $this->success($log_count . '个日志文件已经发送成功，感谢您的支持');
            }
            $this->error('发送日志失败，CODE:' . $response['code'] . '，MSG:' . $response['msg']);
        }
        $this->error('发送日志失败，远程返回数据为空');
    }

    public function packsrc()
    {
        $files = $this->_packsrc_recursion('./asserts/');
        $this->success('Compress for ' . count($files) . ' file(s)');
    }

    private function _packsrc_recursion($dir)
    {
        $returns = array();
        $ignore_file = array('jquery-2.0.0.src.js');
        foreach (list_file($dir) as $f) {
            if ($f ['isDir']) {
                $returns = array_merge($returns, $this->_packsrc_recursion($f ['pathname'] . '/'));
            } else if ($f ['isFile']) {
                if (substr($f ['filename'], -8) == '.src.css') {
                    $pack_path = substr($f ['pathname'], 0, -8) . '.css';
                    if (!file_exists($pack_path) || filemtime($pack_path) < filemtime($f ['pathname'])) {
                        $this->_pack_css($f ['pathname'], $pack_path);
                        $returns[] = $pack_path;
                    }
                } else if (substr($f ['filename'], -7) == '.src.js') {
                    $pack_path = substr($f ['pathname'], 0, -7) . '.js';
                    if (!file_exists($pack_path) || filemtime($pack_path) < filemtime($f ['pathname'])) {
                        if (in_array($f['filename'], $ignore_file)) {
                            @copy($f ['pathname'], $pack_path);
                        } else {
                            $this->_pack_js($f ['pathname'], $pack_path);
                        }
                        $returns[] = $pack_path;
                    }
                }
            }
        }
        return $returns;
    }

    private function  _pack_css($srcpath, $descpath)
    {
        $content = file_get_contents($srcpath);
        $content = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*' . '/!', '', $content);
        $content = str_replace(array(
            "\r\n",
            "\r",
            "\n",
            "\t",
            '  ',
            '    ',
            '    '
        ), '', $content);
        $content = str_replace(array(
            " 0px",
            " 0em",
            ' 0pt'
        ), ' 0', $content);
        $content = preg_replace('/ *; *' . '/', ';', $content);
        $content = preg_replace('/ *: *' . '/', ':', $content);
        $content = preg_replace('/ *\\} *' . '/', '}', $content);
        $content = preg_replace('/ *\\{ *' . '/', '{', $content);
        $content = preg_replace('/; *\\}/', '}', $content);
        file_put_contents($descpath, $content);
    }

    private function _pack_js($src, $des)
    {
        $url = 'http://tool.lu/js/ajax.html';
        import('Common.Extends.Snoopy');
        $snoopy = new \Snoopy();
        $snoopy->agent = "(compatible; MSIE 4.01; MSN 2.5; AOL 4.0; Windows 98)";
        $snoopy->referer = $url;

        $post = array();
        $post['operate'] = 'uglify';
        $post['code'] = file_get_contents($src);
        $t1 = microtime(true);

        if ($snoopy->submit($url, $post)) {
            $json = @json_decode($snoopy->getResults(), true);
            if (!empty($json['text'])) {
                $t2 = microtime(true);
                file_put_contents($des, $json['text']);
            }
        } else {
            $t2 = microtime(true);
        }

        $time = sprintf('%.4f', ($t2 - $t1));
        return $src . " -&gt; " . $des . " cost " . $time . ' s';
    }
}